﻿using NewsAgregator.Models;
using NewsAgregator.Tools.HTMLParsing;
using PuppeteerSharp;

namespace NewsAgregator.Services.Parsers
{
    public static class SingleNewsParser
    {
        /// <summary>
        /// Gets single news from element.
        /// </summary>
        /// <param name="item">Item of the page</param>
        /// <returns>Parsed single news or null</returns>
        public static async Task<News?> TryGetNews(IElementHandle item)
        {
            var itemParse = new ItemParseHandler(item);
            var title = await itemParse.TryGetData("*[class*=\"title\"]", "innerText");
            var shortDescription = await itemParse.TryGetData("*[class*=\"lead\"]", "innerText");
            var date = await itemParse.TryGetData("*[class*=\"date\"]", "innerText");
            var url = await itemParse.TryGetData("a", "href");
            if (url == null)
                url = (await item.EvaluateFunctionAsync("node => node.parentElement.href")).ToString();

            if (title != null && url != null)
            {
                return new News
                {
                    Title = title,
                    ShortDescription = shortDescription,
                    CreationDate = DateHandler.GetDateFromText(date),
                    URL = url
                };
            }
            return null;
        }
    }
}
