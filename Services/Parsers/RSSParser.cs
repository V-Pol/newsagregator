﻿using NewsAgregator.Models;
using NewsAgregator.Tools;
using PuppeteerSharp;
using System.ServiceModel.Syndication;
using System.Xml;

namespace NewsAgregator.Services.Parsers
{
    public class RSSParser : IParser
    {
        /// <summary>
        /// Parses RSS of the news page.
        /// </summary>
        /// <param name="page">Page for parsing</param>
        /// <returns>Task which resolves to IEnumerable of parsed news or empty list</returns>
        public async Task<IEnumerable<News>?> TryGetNewsAsync(IPage page)
        {
            var newsList = new List<News>();
            var rss = await RSSHandler.TryGetRSS(page);
            if (rss != null)
            {
                using XmlReader reader = XmlReader.Create(rss);
                var feed = SyndicationFeed.Load(reader);

                foreach (SyndicationItem item in feed.Items)
                {
                    var news = new News
                    {
                        Title = item.Title.Text,
                        ShortDescription = item.Summary.Text,
                        CreationDate = item.PublishDate.DateTime,
                        URL = item.Id,
                    };
                    newsList.Add(news);
                }
            }
            return newsList;
        }
    }
}
