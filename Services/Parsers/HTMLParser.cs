﻿using NewsAgregator.Models;
using NewsAgregator.Tools.HTMLParsing;
using PuppeteerSharp;

namespace NewsAgregator.Services.Parsers
{
    public class HTMLParser : IParser
    {
        /// <summary>
        /// Parses HTML code of the news page. If no elements was parsed return null.
        /// </summary>
        /// <param name="page">Page for parsing</param>
        /// <returns>Task which resolves to IEnumerable of parsed news or null</returns>
        public async Task<IEnumerable<News>?> TryGetNewsAsync(IPage page)
        {
            var newsList = new List<News>();
            var baseUrl = page.Url;
            string[] newsUrlVariants = 
            { 
                "news", 
                "online", 
                "novosti" 
            };
            foreach (var variant in newsUrlVariants)
            {
                if ((await page.GoToAsync(baseUrl + variant)).Ok)
                {
                    var newsBlocks = await GetNewsBlocks(page);
                    foreach (var item in newsBlocks!)
                    {
                        var news = await SingleNewsParser.TryGetNews(item);
                        if (news != null)
                            newsList.Add(news);
                    }
                    return newsList;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets list of block contains news data from news page. 
        /// </summary>
        /// <param name="page">Page for parsing</param>
        /// <returns>Groping blocks element</returns>
        async Task<IGrouping<string, IElementHandle>?> GetNewsBlocks(IPage page)
        {
            IElementHandle[] maxElements = new IElementHandle[0];
            string[] classSelectors =
                {
                    "item-data",
                    "ListItem",
                    "post"
                };
            foreach (var classSelector in classSelectors)
            {
                var currElements = await page.QuerySelectorAllAsync($"*[class*=\"{classSelector}\"]");
                if (currElements.Count() > maxElements.Count())
                    maxElements = currElements;
            }
            if (maxElements.Count() < 2)
                maxElements = await page.QuerySelectorAllAsync("div");
            return maxElements.GroupBy(ds => ds.RemoteObject.Description).MaxBy(x => x.Count());
        }
    }
}
