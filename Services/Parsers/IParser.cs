﻿using NewsAgregator.Models;
using PuppeteerSharp;

namespace NewsAgregator.Services.Parsers
{
    public interface IParser
    {
        Task<IEnumerable<News>?> TryGetNewsAsync(IPage page);
    }
}
