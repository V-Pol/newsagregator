﻿using NewsAgregator.Models;
using NewsAgregator.Repos;
using NewsAgregator.Services.Parsers;
using NewsAgregator.Tools;

namespace NewsAgregator.Services
{
    public class PageParser
    {
        readonly INewsRepository repo;
        readonly IEnumerable<IParser> parsers;
        public PageParser(INewsRepository repo, IEnumerable<IParser> parsers)
        {
            this.repo = repo;
            this.parsers = parsers;
        }

        /// <summary>
        /// Parses the page of news website by url and adds parsing data in DB
        /// </summary>
        /// <param name="url">Url of the news website</param>
        public async Task UrlParse(Uri url)
        {
            url = new UriBuilder(url.Scheme, url.Host).Uri;
            var browser = new BrowserManager().browser;
            using var page = await browser.NewPageAsync();
            await page.GoToAsync(url.ToString());

            IEnumerable<News>? newsList = new List<News>();

            foreach(var parser in parsers)
            {
                newsList = await parser.TryGetNewsAsync(page);
                if (newsList!.Count() != 0)
                    break;
            }
            
            if (newsList != null)
                await repo.AddRangeAsync(newsList);

            await page.CloseAsync();
            await browser.CloseAsync();
            await browser.DisposeAsync();
        }
    }
}