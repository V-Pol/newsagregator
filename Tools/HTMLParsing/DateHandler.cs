﻿using System.Text.RegularExpressions;

namespace NewsAgregator.Tools.HTMLParsing
{
    public static class DateHandler
    {
        /// <summary>
        /// Gets date from simple text
        /// </summary>
        /// <param name="text">String with text date</param>
        /// <returns>Date of news</returns>
        public static DateTime GetDateFromText(string? text)
        {
            if (CheckDateTime(text))
            {
                if (text.Contains("Сегодня", StringComparison.OrdinalIgnoreCase))
                    return DateTime.Today;
                if (text.Contains("Вчера", StringComparison.OrdinalIgnoreCase))
                    return DateTime.Today.AddDays(-1);
                return DateTime.Parse(text);
            }
            else return DateTime.Today;
        }

        private static bool CheckDateTime(string text)
        {
            Regex timeRegex = new Regex(@"([0-1]?[0-9]|2[0-3]):[0-5][0-9]$");
            Regex timeRegex2 = new Regex(@"(0?[0-9]|[1-5][0-9])\s*\w*минут");
            Regex timeRegex3 = new Regex(@"(0?[0-9]|1[0-9]|2[0-3])\s*\w*час");
            return timeRegex.IsMatch(text) || timeRegex2.IsMatch(text) || timeRegex3.IsMatch(text);
        }
    }
}
