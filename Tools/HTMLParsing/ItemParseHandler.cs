﻿using PuppeteerSharp;

namespace NewsAgregator.Tools.HTMLParsing
{
    public class ItemParseHandler
    {
        private readonly IElementHandle item;
        public ItemParseHandler(IElementHandle item)
        {
            this.item = item;
        }

        /// <summary>
        /// Gets string data from item.
        /// </summary>
        /// <param name="selector">A selector to query element for.</param>
        /// <param name="attribute">A single property from the referenced object.</param>
        /// <returns>String data from element property or null.</returns>
        public async Task<string?> TryGetData(string selector, string attribute)
        {
            var element = await item.QuerySelectorAsync(selector);
            if (element != null)
            {
                return (await element.GetPropertyAsync(attribute)).RemoteObject.Value?.ToString();
            }
            return null;
        }
    }
}
