﻿using PuppeteerSharp;
using System;

namespace NewsAgregator.Tools
{
    public class BrowserManager
    {
        public IBrowser browser { get; }

        public BrowserManager()
        {
            using var browserFetcher = new BrowserFetcher();

            using var browser = Puppeteer.LaunchAsync(new LaunchOptions
            {
                Browser = SupportedBrowser.Chromium,
                Headless = true,
                ExecutablePath = @"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe"
            });

            this.browser = browser.Result;
        }
    }
}
