﻿using PuppeteerSharp;

namespace NewsAgregator.Tools
{
    public static class RSSHandler
    {
        /// <summary>
        /// Gets RSS URL from page.
        /// </summary>
        /// <param name="page">Page of the website.</param>
        /// <returns>URLs string of the RSS</returns>
        public static async Task<string?> TryGetRSS(IPage page)
        {
            var rss = await page.QuerySelectorAsync("link[type*=\"rss\"]");
            if (rss != null)
                return (await rss.GetPropertyAsync("href")).RemoteObject.Value.ToString();
            return null;
        }
    }
}
