﻿namespace NewsAgregator.Tools
{
    public static class UrlHandler
    {
        public static Uri GetDecodeUri(string url)
        {
            //Проблема https://learn.microsoft.com/ru-ru/aspnet/core/fundamentals/routing?view=aspnetcore-7.0#routing-with-special-characters
            url = System.Web.HttpUtility.UrlDecode(url);
            return new UriBuilder(url).Uri;
        }
    }
}
