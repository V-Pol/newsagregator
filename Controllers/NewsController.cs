using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using NewsAgregator.Repos;
using NewsAgregator.Models;
using NewsAgregator.Services;
using Hangfire;
using NewsAgregator.Tools;
using System.Runtime.CompilerServices;

namespace NewsAgregator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NewsController : ControllerBase
    {
        private readonly INewsRepository repo;
        private readonly PageParser pageParser;
        public NewsController(INewsRepository repo, PageParser pageParser)
        {
            this.repo = repo;
            this.pageParser = pageParser;
        }

        /// <summary>
        /// Gets all news
        /// </summary>
        /// <returns>All news</returns>
        /// <response code="200">All news from DB</response>
        /// <response code="204">No content</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [SwaggerResponse(200, "The execution was successful")]
        [SwaggerResponse(204, "No content")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<News>>> GetAll()
        {
            var news = await repo.GetAllAsync();
            if (news.Count() > 0)
                return Ok(news);
            return NoContent();
        }

        /// <summary>
        /// Find news by text in a title
        /// </summary>
        /// <param name="text">Text for find</param>
        /// <returns>List of the news</returns>
        /// <response code="200">Found news</response>
        /// <response code="204">No content</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [SwaggerResponse(200, "The execution was successful")]
        [SwaggerResponse(204, "No content")]
        [HttpGet("{text}")]
        public async Task<ActionResult<IEnumerable<News>>> FindAsync(string text)
        {
            var news = await repo.FindAsync(text);
            if (news.Count() > 0)
                return Ok(news);
            return NoContent();
        }

        /// <summary>
        /// Add the news URL for tracking
        /// </summary>
        /// <remarks>
        /// Example:
        ///
        ///     PUT
        ///     {
        ///        "url" : https://newswebsite.com
        ///     }
        ///
        /// </remarks>
        /// <param name="url">The URL of a news website</param>
        /// <returns>The status of the addition</returns>
        /// <response code="200">Successfully added</response>
        /// <response code="400">Incorrect URL</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [SwaggerResponse(200, "The URL addition was successful")]
        [SwaggerResponse(400, "The URL was incorrect")]
        [HttpPut("{url}")]
        public ActionResult AddUrl(string url)
        {
            var uri = UrlHandler.GetDecodeUri(url);
            if (uri.IsAbsoluteUri)
            {
                RecurringJob.AddOrUpdate(uri.Host, () => pageParser.UrlParse(uri), "@hourly");
                return Ok();
            }
            return BadRequest();
        }

        /// <summary>
        /// Delete the news URL from tracking
        /// </summary>
        /// <remarks>
        /// Example:
        ///
        ///     DELETE
        ///     {
        ///        "url" : https://newswebsite.com
        ///     }
        ///
        /// </remarks>
        /// <param name="url">The URL of a news website</param>
        /// <returns>The status of the deletion</returns>
        /// <response code="200">Successfully deleted</response>
        /// <response code="400">Incorrect URL</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [SwaggerResponse(200, "The URL addition was successful")]
        [SwaggerResponse(400, "The URL was incorrect")]
        [HttpDelete("{url}")]
        public ActionResult DeleteUrl(string url)
        {
            var uri = UrlHandler.GetDecodeUri(url);
            if (uri.IsAbsoluteUri)
            {
                RecurringJob.RemoveIfExists(uri.Host);
                return Ok();
            }
            return BadRequest();
        }
    }
}