using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NewsAgregator.Repos;
using NewsAgregator.Services;
using NewsAgregator.Services.Parsers;
using PuppeteerSharp;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("NewsDB");

var services = builder.Services;

services.AddControllers();
services.AddEndpointsApiExplorer();
services.AddScoped<INewsRepository, NewsRepository>(provider => new NewsRepository(connectionString!));
services.AddScoped<IParser, RSSParser>();
services.AddScoped<IParser, HTMLParser>();
services.AddScoped<PageParser>();
services.AddHangfire(config => config.UsePostgreSqlStorage(config =>
        config.UseNpgsqlConnection(connectionString)));
services.AddHangfireServer();
services.AddSwaggerGen(options =>
{
    options.EnableAnnotations();
    options.SwaggerDoc("v1",
        new OpenApiInfo
        {
            Title = "News Agregator Service",
            Version = "v1",
            Description = "������ ��� ��������� ��������",
            Contact = new OpenApiContact
            {
                Name = "��������� ��������� ���������",
                Url = new Uri("https://v-pol.gitlab.io/representationsite"),
            },
        });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();
app.UseHangfireDashboard();
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();