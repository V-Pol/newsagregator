﻿using Dapper;
using NewsAgregator.Models;
using Npgsql;

namespace NewsAgregator.Repos
{
    public class NewsRepository : INewsRepository
    {
        private readonly NpgsqlConnection db;
        const string insertCommand =
            $"INSERT INTO News (Title, ShortDescription, CreationDate, Url) VALUES (@title, @shortDescription, @creationDate, @url) ON CONFLICT (Url) DO NOTHING;";
        
        public NewsRepository(string connectionString)
        {
            db = new NpgsqlConnection(connectionString);
        }

        public async Task AddAsync(News news) =>
            await db.ExecuteAsync(insertCommand, news);

        public async Task AddRangeAsync(IEnumerable<News> news) =>
            await db.ExecuteAsync(insertCommand, news.ToList<object>());

        public async Task<IEnumerable<News>> FindAsync(string text)
        {
            string commandText = $"SELECT * FROM News WHERE Title ILIKE '%' || @text || '%';";
            var queryArgs = new { text };
            return await db.QueryAsync<News>(commandText, queryArgs);
        }

        public async Task<IEnumerable<News>> GetAllAsync()
        {
            string commandText = $"SELECT * FROM News;";
            return await db.QueryAsync<News>(commandText);
        }
    }
}