﻿using NewsAgregator.Models;

namespace NewsAgregator.Repos
{
    public interface INewsRepository
    {
        Task AddAsync(News news);
        Task AddRangeAsync(IEnumerable<News> news);
        Task<IEnumerable<News>> FindAsync(string text);
        Task<IEnumerable<News>> GetAllAsync();
    }
}
