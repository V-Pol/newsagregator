﻿namespace NewsAgregator.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string? ShortDescription { get; set; } = null;
        public DateTime CreationDate { get; set; } = DateTime.Today;
        public string URL { get; set; } = string.Empty;
    }
}
